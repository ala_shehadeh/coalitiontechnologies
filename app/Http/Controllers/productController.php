<?php

namespace App\Http\Controllers;

use App\products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class productController extends Controller
{
    function __construct()
    {
        $this->product = new products();
    }

    function store(Request $request)
    {
        //add article validation
        $rules = [
            'product_name' => 'required|unique:products,product_name|min:3',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric'
        ];
        $input = $request->only(
            'product_name',
            'quantity',
            'price'
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails())
            return view('added')->with('error', $validator->errors()->all());
        else {
            $this->product->store($request);
            return view('added')->with('success', true);
        }
    }

    function all()
    {
        return view('products')->with('products', $this->product->all());
    }
}
