<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class products extends Model
{
    protected $table = 'products';

    function store($data)
    {
        $this->insert([
            'product_name' => $data->product_name,
            'quantity' => $data->quantity,
            'price' => $data->price,
            'created_at' => DB::raw('now()')
        ]);
    }

    function allData()
    {
        return $this->orderBy('created_at');
    }
}
