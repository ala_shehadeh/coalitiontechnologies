@yield('form')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Coalition Skills test</title>

    <!-- css files -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link href="css/app.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form>
                <!-- product name -->
                <div class="form-group">
                    <label>Product name</label>
                    <input type="text" id="name" class="form-control" required />
                </div>

                <!-- Quantity in stock -->
                <div class="form-group">
                    <label>Quantity in stock</label>
                    <input type="number" id="stock" class="form-control" required />
                </div>

                <!-- Price per item -->
                <div class="form-group">
                    <label>Price per item</label>
                    <input type="number" step="0.01" id="price" class="form-control" required />
                </div>

                <!-- submit button -->
                <div class="form-group text-center">
                    <button id="add" type="button" class="btn btn-primary">Submit</button>
                </div>
            </form>

            <!-- add result -->
            <div id="result"></div>

            <!-- product data -->
            <div id="products"></div>
        </div>
    </div>
</div>

<!-- JS files -->
<script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="js/products.js"></script>

</body>
</html>